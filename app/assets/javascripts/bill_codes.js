$('#tbl_bill_codes').dataTable({
  "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",                                                                                                    
  "sPaginationType": "bootstrap",
  "bLengthChange": false,
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>',
  "bServerSide": true,
  "bProcessing": true,
  "sAjaxSource": $('#tbl_bill_codes').data('source'),
  "fnDrawCallback" : function() {
    $('.editable').editable();
    $('#tbl_bill_codes thead th:last').css('width','36px');
    $('#tbl_bill_codes thead th:first').css('width','148px'); 
    $('#row_counter').text($('#tbl_bill_codes').dataTable().fnSettings().fnRecordsTotal())
    $('.toggle').toggles({
        checkbox:$('.check_box'),
        //type:'select',
        style: 'modern',
        width: 70, 
        height: 30       
    });     
    $('.toggle-active').toggles({
        checkbox:$('.check_box'),
        //type:'select',
        style: 'modern',
        on: true,
        width: 70, 
        height: 30       
    });  
    
  }  
})

$('#tbl_form tr td:last').css('width','50px');        
$('#tbl_form tr td:first').css('width','180px');



$('.modal .modal-footer :submit').button()
$('.modal .modal-footer :submit').on("click", function () {
		$(this).button('loading')
});


