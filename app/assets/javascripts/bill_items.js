$('#tbl_bill_items').dataTable({
  "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",                                                                                                    
  "sPaginationType": "bootstrap",
  "bLengthChange": false,
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>',
  "bServerSide": true,
  "bProcessing": true,
  "sAjaxSource": $('#tbl_bill_items').data('source'),
  "fnDrawCallback" : function() {
    $('.editable').editable(); 
    $('#tbl_bill_items thead th:last').css('width','190px');
    $('#row_counter').text($('#tbl_bill_items').dataTable().fnSettings().fnRecordsTotal())
  }  
})

