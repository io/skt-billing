$('#tbl_bills').dataTable({
  "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",                                                                                                    
  "sPaginationType": "bootstrap",
  "bLengthChange": false,
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>',
  "bServerSide": true,
  "bProcessing": true,
  "sAjaxSource": $('#tbl_bills').data('source'),
  "fnDrawCallback" : function() {
    $('.editable').editable(); 
    $('#row_counter').text($('#tbl_bills').dataTable().fnSettings().fnRecordsTotal())
  }  
})    

   

    
$("tbody tr").on("click", function(e) {
    if ($(e.target).is("a,input"))
        return;
    location.href = $(this).find("a").attr("href");
    e.stopPropagation();
});
