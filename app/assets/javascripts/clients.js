$('#tbl_clients').dataTable({
  "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",                                                                                                    
  "sPaginationType": "bootstrap",
  "bLengthChange": false,
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>',
  "bServerSide": true,
  "bProcessing": true,
  "sAjaxSource": $('#tbl_clients').data('source'),
  "fnDrawCallback" : function() {
    $('.editable').editable();
    $('#row_counter').text($('#tbl_clients').dataTable().fnSettings().fnRecordsTotal())
  }  
})

