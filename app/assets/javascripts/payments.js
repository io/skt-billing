$('#tbl_payments').dataTable({
  "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",                                                                                                    
  "sPaginationType": "bootstrap",
  "bLengthChange": false,
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>'
})

$('#row_counter').text($('#tbl_payments').dataTable().fnSettings().fnRecordsTotal())

$("tbody tr").on("click", function(e) {
    if ($(e.target).is("a,input"))
        return;
    location.href = $(this).find("a").attr("href");
    e.stopPropagation();
});
