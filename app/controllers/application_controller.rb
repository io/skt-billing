class ApplicationController < ActionController::Base
  	# ---------------------------------------------------------------------- 
  	before_filter :authenticate_user!
  	after_filter :flash_to_headers
  	protect_from_forgery                                                    
  	# ----------------------------------------------------------------------
  	rescue_from CanCan::AccessDenied do |exception|                         
  	  redirect_to main_app.root_path, :alert => exception.message                    
  	end                                                                     
  	# ----------------------------------------------------------------------
  	# before_filter :set_layout		                       
  	# before_filter :set_locale
        def flash_to_headers
          if (request.xhr?)
            response.headers['X-Message'] = flash_message
            response.headers["X-Message-Type"] = flash_type.to_s
            flash.discard
          end
        end  	
  	# ----------------------------------------------------------------------
  	private   
          def flash_message
              [:error, :warning, :notice].each do |type|
                  return flash[type] unless flash[type].blank?
              end   
              return ''
          end
          
          def flash_type
              [:error, :warning, :notice].each do |type|
                  return type unless flash[type].blank?
              end
          end  	
  	  # --------------------------------------------------------------------
  	  #                                                      
  	  # --------------------------------------------------------------------
  	  def set_locale                                                                         
  	   # https://github.com/pigats/48rails
  	   http_requested_lang = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^(#{I18n.available_locales.join('|')})/)[0][0] unless request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^(#{I18n.available_locales.join('|')})/).empty? or request.env['HTTP_ACCEPT_LANGUAGE'].nil?     
  	   I18n.locale = params[:lang] || http_requested_lang || I18n.default_locale
  	   I18n.locale = params[:locale] || ((lang = request.env['HTTP_ACCEPT_LANGUAGE']) && lang[/^[a-z]{2}/])
  	  end
  	  # --------------------------------------------------------------------  
  	  # Extract Locale from TLD
  	  # In /etc/hosts:
  	  #   127.0.0.1 application.com
  	  #   127.0.0.1 application.it
  	  #   127.0.0.1 application.pl
  	  # --------------------------------------------------------------------
  	  def extract_locale_from_tld
  	    parsed_locale = request.host.split('.').last
  	    I18n.available_locales.include?(parsed_locale.to_sym) ? parsed_locale  : nil
  	  end
  	  # --------------------------------------------------------------------    
  	  # Extract Locale from Subdomain, e.g. http://it.application.local:3000
  	  # In /etc/hosts: 127.0.0.1 gr.application.local        
  	  # --------------------------------------------------------------------
  	  def extract_locale_from_subdomain
  	    parsed_locale = request.subdomains.first
  	    I18n.available_locales.include?(parsed_locale.to_sym) ? parsed_locale : nil
  	  end    
  	  # --------------------------------------------------------------------
  	  # Pjax Interop
  	  # --------------------------------------------------------------------
  	  def set_layout
  	    if request.headers['X-PJAX']
  	      false
  	    end
  	  end
  	  # -------------------------------------------------------------------- 
end
