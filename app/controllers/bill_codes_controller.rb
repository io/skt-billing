class BillCodesController < ApplicationController
	# ---------------------------------------------------------------------- 
  	before_filter :authenticate_user! 
  	load_and_authorize_resource
	# ---------------------------------------------------------------------- 
	respond_to :html, :json, :js
	has_scope :active 
        # ---------------------------------------------------------------------- 
  	def index
  	  @bill_code = BillCode.new 
  	  respond_with do |format|
  	    format.json { render json: BillCodesDatatable.new(view_context) }
  	  end                                                                                         
  	end         
 	# ----------------------------------------------------------------------  
  	def create  
  	  @bill_code = BillCode.new(params[:bill_code])  	  
  	  respond_to do |format|
  	    if @bill_code.save
  	      flash[:notice] = "Bill Code was successfully created."
  	      format.js {}
  	      format.json { render json: @bill_code, status: :created, location: @bill_code }
  	    else
  	      #format.html { render action: "new" }
  	      messages = @bill_code.errors.full_messages.map { |msg| flash.now[:error] = msg }.join 
  	      format.json { render json: messages.titleize, status: :unprocessable_entity }
  	    end
  	  end    
  	end  
        # ---------------------------------------------------------------------- 
  	def update    
  	  @bill_code = BillCode.find(params[:id])  
  	  @bill_code.update_attributes(params[:bill_code])     
  	  flash[:notice] = "Bill Code was successfully updated." if @bill_code.save
  	  respond_with @bill_code
  	end   
  	# ---------------------------------------------------------------------- 
  	def destroy             
  	  @bill_code = BillCode.find(params[:id])
  	  respond_to do |format|
  	    if @bill_code.destroy
  	      flash[:notice] = "Bill Code was successfully deleted."
  	      format.js {}
  	      format.json { render json: BillCodesDatatable.new(view_context) }
  	    else
  	      format.html { render action: "index" }
  	      format.json { render json: @bill_code.errors, status: :unprocessable_entity }
  	    end
  	  end    
  	end   	
  	# ---------------------------------------------------------------------- 
end
