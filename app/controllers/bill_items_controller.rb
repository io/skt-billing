class BillItemsController < InheritedResources::Base
	# ---------------------------------------------------------------------- 
  	before_filter :authenticate_user! 
  	load_and_authorize_resource
	# ---------------------------------------------------------------------- 	
	respond_to :html, :json
        # ---------------------------------------------------------------------- 	
  	def index
  	  respond_with do |format|
  	    format.json { render json: BillItemsDatatable.new(view_context) }  
  	  end                                                                                         
  	end 
	# ---------------------------------------------------------------------- 
end
