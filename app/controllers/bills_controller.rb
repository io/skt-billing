class BillsController < InheritedResources::Base
	# ---------------------------------------------------------------------- 
  	before_filter :authenticate_user! 
  	load_and_authorize_resource
	# ---------------------------------------------------------------------- 	
	layout "pdf", :only => [:show]  
	respond_to :html, :json, :js     
	# ---------------------------------------------------------------------- 
  	def index
  	  respond_with do |format|
  	    format.json { render json: BillsDatatable.new(view_context) }  	                                       
  	  end                                                                                         
  	end 		
        # ---------------------------------------------------------------------- 
  	def show
  	  @bill = Bill.find(params[:id])   	 
  	  respond_with(@bill) do |format|
  	    format.pdf {}  	                                       
  	  end                                                                                         
  	end         
  	# ----------------------------------------------------------------------   
end
