class ClientsController < InheritedResources::Base
	# ---------------------------------------------------------------------- 
  	before_filter :authenticate_user! 
  	load_and_authorize_resource
	# ---------------------------------------------------------------------- 	
	respond_to :html, :json, :js
        # ---------------------------------------------------------------------- 	
  	def index
  	  respond_with do |format|
  	    format.json { render json: ClientsDatatable.new(view_context) } 
  	  end                                                                                         
  	end 	
        # ---------------------------------------------------------------------- 	
  	def update                   
  	  @client = Client.find(params[:id])  
  	  @client.update_attributes(params[:client])
  	  flash[:notice] = "Client was successfully updated." if @client.save
  	  respond_with @client  
  	end 	
	# ---------------------------------------------------------------------- 
end
