class CompaniesController < InheritedResources::Base
	# ---------------------------------------------------------------------- 
  	before_filter :authenticate_user! 
  	load_and_authorize_resource
	# ---------------------------------------------------------------------- 	
	respond_to :html, :json
        # ---------------------------------------------------------------------- 	
  	def index
  	  respond_with do |format|
  	    format.json { render json: CompaniesDatatable.new(view_context) } 
  	  end                                                                                         
  	end 	
	# ---------------------------------------------------------------------- 
  	def update                   
  	  @company = Company.find(params[:id])  
  	  @company.update_attributes(params[:company])
  	  flash[:notice] = "Company was successfully updated." if @company.save
  	  respond_with @company  
  	end 	
	# ---------------------------------------------------------------------- 
end
