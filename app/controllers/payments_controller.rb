class PaymentsController < InheritedResources::Base
	# ---------------------------------------------------------------------- 
  	before_filter :authenticate_user! 
  	load_and_authorize_resource
	# ---------------------------------------------------------------------- 	
	def index
		@payments = Payment.valid
	end
	# ---------------------------------------------------------------------- 
end
