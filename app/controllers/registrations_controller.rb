class RegistrationsController < Devise::RegistrationsController
	# **********************************************************************
	# Actions                                                               
	# **********************************************************************
	# ----------------------------------------------------------------------
	# ovverride #create to respond to AJAX with a partial                   
	# ----------------------------------------------------------------------
	def create                                                              
	  if (params[:flash]=='true')                                           
	    @view = 'thankyou_flash'
	  else
	    @view = 'thankyou'
	  end
	  build_resource
	  if resource.save
	    if resource.active_for_authentication?
	      sign_in(resource_name, resource)
	      (render(:partial => @view, :layout => false) && return) if (request.xhr?)        
	      respond_with resource, :location => after_sign_up_path_for(resource)
	    else
	      expire_session_data_after_sign_in!
	      (render(:partial => @view, :layout => false) && return) if (request.xhr?)        
	      respond_with resource, :location => after_inactive_sign_up_path_for(resource)
	    end
	  else
	    clean_up_passwords resource
	    messages = resource.errors.full_messages.map { |msg| flash.now[:error] = msg }.join	        
	    if (params[:flash]=='true')
	      render(:partial => 'alerts', :layout => false)
	    else
	      render :action => :new, :layout => !request.xhr?
            end    
	  end
	end                                                                           
	# **********************************************************************
	# Private                                                               
	# **********************************************************************
	protected                                                               
	  # --------------------------------------------------------------------
	  # This is rendered if AJAX is not used                                
	  # --------------------------------------------------------------------
	  def after_inactive_sign_up_path_for(resource)                         
	    '/thankyou'                                                         
	  end                                                                   
	  # --------------------------------------------------------------------
	  def after_sign_up_path_for(resource)                                  
	    # the page new users will see after sign up (after launch, when no invitation is needed)
	    redirect_to root_path
	  end
	  # --------------------------------------------------------------------
end
