class UsersController < ApplicationController
  	# **********************************************************************
  	#                                                                 
  	# **********************************************************************
        respond_to :html, :json, :js
        load_and_authorize_resource
  	# **********************************************************************
  	#                                                                
  	# **********************************************************************
  	def index
  	  respond_with do |format|
  	    format.json { render json: UsersDatatable.new(view_context) }   
  	  end
  	end                                                                     
end
