class BillCodesDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: BillCode.count,
      iTotalDisplayRecords: bill_codes.total_entries,
      aaData: data
    }
  end

private

  def data
    bill_codes.map do |bill_code|
      [
        link_to(bill_code.bill_code, "#", :class => "editable", :data => {:type => "text", :model => "bill_code", :name => "bill_code", :url => "/bill_codes/#{bill_code.id}"}),
        h((sprintf '%05d', bill_code.bill_code_start_from)),
        #(bill_code.bill_code_status.to_i == 0) ? h("") : h("Active"),
        if (bill_code.bill_code_status.to_i == 1) 
        	link_to("#", :class => "pull-right btn btn-limekiln-3", :data => {:toggle => "modal", :target => "#bill_code_deactivate_#{bill_code.id}"} ) do
        		("<i class='font-awesome icon-check-sign'></i>").html_safe 
        	end
        else
        	""
        end, 
        link_to("#", :class => "pull-right btn btn-hem-5", :data => {:toggle => "modal", :target => "#bill_code_#{bill_code.id}"} ) do
        	("<i class='font-awesome icon-trash'></i>").html_safe
        end
        
      ]
    end
  end

  def bill_codes
    @bill_codes ||= fetch_bill_codes
  end

  def fetch_bill_codes
    bill_codes = BillCode.order("#{sort_column} #{sort_direction}")
    bill_codes = bill_codes.page(page).per_page(per_page)
    if params[:sSearch].present?
      bill_codes = bill_codes.where("bill_code like :search or bill_code_start_from like :search", search: "%#{params[:sSearch]}%")
    end
    bill_codes
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[bill_code bill_code_start_from]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
