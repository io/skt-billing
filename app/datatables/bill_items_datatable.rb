class BillItemsDatatable
  delegate :params, :h, :link_to, :distance_of_time_in_words, :number_to_currency, :number_to_percentage, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: BillItem.count,
      iTotalDisplayRecords: bill_items.total_entries,
      aaData: data
    }
  end

private

  def data
    bill_items.map do |bill_item|
      [
        number_to_currency(bill_item.bill_item_unit_cost, :separator => ",", :precision => 2, :unit => ""),
        number_to_currency(bill_item.bill_item_unit_gross, :separator => ",", :precision => 2, :unit => ""),
        number_to_currency(bill_item.bill_item_unit_fee, :separator => ",", :precision => 2, :unit => ""),
        number_to_percentage(bill_item.bill_item_vat, :separator => ",", :precision => 2),
        h(bill_item.bill_item_currency),
        h(bill_item.bill_item_description),
        distance_of_time_in_words(Time.zone.now, bill_item.bill_item_processed_at).capitalize + " ago",
        link_to(Client.find(bill_item.client_id).client_name.titleize, "#", :class => "editable", :data => {:type => "text", :model => "client", :name => "client_name", :url => "/clients/#{Client.find(bill_item.client_id).id}"}),
        link_to(Company.find(bill_item.company_id).company_name.titleize, "#", :class => "editable", :data => {:type => "text", :model => "company", :name => "company_name", :url => "/companies/#{Company.find(bill_item.company_id).id}"})
        #('<span class="label label-info">' + bill_item.bill_item_transaction_id.to_s + '</span>').html_safe,
        #link_to("Bill", "/bill_items/#{bill_item.id}", :method => :delete, :class => "pull-right btn btn-ultraviolet-rays-4")
        
      ]
    end
  end

  def bill_items
    @bill_items ||= fetch_bill_items
  end

  def fetch_bill_items
    bill_items = BillItem.order("#{sort_column} #{sort_direction}")
    bill_items = bill_items.page(page).per_page(per_page)
    if params[:sSearch].present?
      bill_items = bill_items.where("bill_item_description like :search or bill_item_transaction_id like :search", search: "%#{params[:sSearch]}%")
    end
    bill_items
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[bill_item_unit_cost bill_item_description bill_item_processed_at client_id company_id bill_item_transaction_id]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
