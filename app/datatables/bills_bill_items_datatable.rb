class BillsBillItemsDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: BillItem.count,
      iTotalDisplayRecords: bill_items.total_entries,
      aaData: data
    }
  end

private

  def data
    bill_items.map do |bill_item|
      [
        link_to(bill_item.bill_item_description, "#", :class => "editable", :data => {:type => "text", :model => "bill_item", :name => "bill_item_description", :url => "/bill_items/#{bill_item.id}"}),
        h(bill_item.bill_item_unit_cost),
        h(bill_item.bill_item_currency),
        h(Client.find(bill_item.client_id).client_country),
        link_to(Client.find(bill_item.client_id).client_name.titleize, "#", :class => "editable", :data => {:type => "text", :model => "client", :name => "client_name", :url => "/clients/#{Client.find(bill_item.client_id).id}"}),
        link_to(Company.find(bill_item.company_id).company_name.titleize, "#", :class => "editable", :data => {:type => "text", :model => "company", :name => "company_name", :url => "/companies/#{Company.find(bill_item.company_id).id}"}),
        link_to("Bill", "/bill_items/#{bill_item.id}", :method => :delete, :class => "pull-right btn btn-ultraviolet-rays-4")
        
      ]
    end
  end

  def bill_items
    @bill_items ||= fetch_bill_items
  end

  def fetch_bill_items
    bill_items = BillItem.order("#{sort_column} #{sort_direction}")
    bill_items = bill_items.page(page).per_page(per_page)
    if params[:sSearch].present?
      bill_items = bill_items.where("company_id = :search and bill_id = -1", search: Company.find(params[:sSearch]).id)
    else
      bill_items = bill_items.where("company_id = 1 and bill_id = 0")
    end
    bill_items
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 30
  end

  def sort_column
    columns = %w[bill_item_description bill_item_unit_cost bill_item_currency bill_item_transaction_id client_name company_name]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
