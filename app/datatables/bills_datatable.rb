class BillsDatatable
  delegate :params, :h, :link_to, :distance_of_time_in_words, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Bill.count,
      iTotalDisplayRecords: bills.total_entries,
      aaData: data
    }
  end

private

  def data
    bills.map do |bill|
      [
        link_to(Company.find(bill.company_id).company_country.to_s + "-" + bill.bill_code.bill_code.to_s + "-" + (sprintf '%05d', bill.bill_number), "/bills/#{bill.id}.pdf"),
        number_to_currency(bill.bill_amount, :separator => ",", :precision => 2, :unit => ""),
        h(bill.bill_items.first.bill_item_currency.to_s),
        h(bill.company.company_email),
        distance_of_time_in_words(Time.zone.now, bill.created_at).capitalize + " ago"       
      ]
    end
  end

  def bills
    @bills ||= fetch_bills
  end

  def fetch_bills
    bills = Bill.order("#{sort_column} #{sort_direction}")
    bills = bills.page(page).per_page(per_page)
    if params[:sSearch].present?
      bills = bills.where("bill_recipient_email like :search or bill_code like :search or bill_number like :search", search: "%#{params[:sSearch]}%")
    end
    bills
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[bill_number bill_amount bill_recipient_email bill_sent_at]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
