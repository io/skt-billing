class CompaniesDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Company.count,
      iTotalDisplayRecords: companies.total_entries,
      aaData: data
    }
  end

private

  def data
    companies.map do |company|
      [
        link_to(company.company_name.titleize, "#", :class => "editable", :data => {:type => "text", :model => "company", :name => "company_name", :url => "/companies/#{company.id}"}),        
        link_to(company.company_address, "#", :class => "editable", :data => {:type => "text", :model => "company", :name => "company_address", :url => "/companies/#{company.id}"}),
        link_to(company.company_email, "#", :class => "editable", :data => {:type => "text", :model => "company", :name => "company_email", :url => "/companies/#{company.id}"}),
        #link_to(company.company_phone, "#", :class => "editable", :data => {:type => "text", :model => "company", :name => "company_phone", :url => "/companies/#{company.id}"}),
        link_to(company.company_vat, "#", :class => "editable", :data => {:type => "text", :model => "company", :name => "company_vat", :url => "/companies/#{company.id}"}),
        h(company.company_country)
      ]
    end
  end

  def companies
    @companies ||= fetch_companies
  end

  def fetch_companies
    companies = Company.order("#{sort_column} #{sort_direction}")
    companies = companies.page(page).per_page(per_page)
    if params[:sSearch].present?
      companies = companies.where("company_name like :search or company_address like :search or company_email like :search or company_phone like :search or company_vat like :search", search: "%#{params[:sSearch]}%")
    end
    companies
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[company_name company_address company_email company_vat company_country]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
