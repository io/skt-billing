class UserMailer < ActionMailer::Base
	
  default :from => ENV["USER_EMAIL"]  
  default :bcc => ENV["ADMIN_EMAIL"]
  
  def invoice(bill)
  	  @bill = bill
  	  if Rails.env.development?
  	    send_to = ENV["TESTER_EMAIL"]
  	    send_cc = ""
  	  else    
  	   send_to = [bill.company.company_email, bill.bill_items.first.client.client_email]
  	   send_cc = ENV["USER_EMAIL"]
          end                                                            
         
          mail(:to => send_to, :cc => send_cc, :subject => "Invoice #{ @bill.code }") do |format|          
             format.html
             format.pdf do
               attachments[bill.code.to_s + '.pdf'] = WickedPdf.new.pdf_from_string( render_to_string(:pdf => "bill", :template => 'bills/show.html.haml', :layout => "pdf.html", :handlers => [:haml]) )
             end
          end   
  end 
  
  def activity_report()
  	  if Rails.env.development?
  	    send_to = ENV["TESTER_EMAIL"]
  	    send_cc = ""
  	  else    
  	   send_to = [bill.company.company_email, bill.bill_items.first.client.client_email]
  	   send_cc = ENV["OWNER_EMAIL"]
          end                                                            
         
          mail(:to => send_to, :cc => send_cc, :subject => "Telefree Billing - Acvitity Report - #{ Time.zone.now.strftime('%d/%m/%y') }") do |format|          
             format.html
          end   
  end   
  
  
end
