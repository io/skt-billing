class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    alias_action :create, :read, :update, :destroy, :to => :crud
    
    if user.has_role? :admin
    	    can :manage, :all
    elsif user.has_role? :owner
    	    can :manage, [Company, Client, BillItem, Bill, BillCode, User]
    	    cannot :manage, [Account, Payment]
    else
    	    can :manage, [Company, Client, BillItem, Bill] 
    	    cannot :manage, [BillCode, User, Account, Payment]
    end
  end
end
