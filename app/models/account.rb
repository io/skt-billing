class Account < ActiveRecord::Base            
	# ----------------------------------------------------------------------
	has_many :payments
	belongs_to :address
	belongs_to :currency
	has_many :bills
	# ----------------------------------------------------------------------
	def self.valid
		Account.where{(first_name.not_eq '') & (vat_number.not_eq '')}
	end
	# ---------------------------------------------------------------------- 	
end
