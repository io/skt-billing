class Bill < ActiveRecord::Base       
	# ----------------------------------------------------------------------
  	attr_accessible :company_id, :bill_amount, :bill_code_id, :bill_status, :bill_sent_at
  	# ----------------------------------------------------------------------
  	validates :company_id, :presence => true
  	validates :bill_status, :inclusion => { :in => ['draft', 'sent'] }
  	# ----------------------------------------------------------------------
  	belongs_to :company
  	belongs_to :bill_code
  	has_many :bill_items 
  	accepts_nested_attributes_for :bill_items, :allow_destroy => true 
  	# ----------------------------------------------------------------------
  	default_values :bill_notes => { :value => '', :allows_nil => false },
  			:bill_status => { :value => 'draft', :allows_nil => false },
  			:bill_due_date => { :value => lambda { Time.zone.now.strftime("%d-%m-%Y %H:%M") }, :allows_nil => false },
  			:bill_code_id => { :value => lambda { BillCode.active.first.id }, :allows_nil => false },
  			:bill_vat => { :value => 21.0, :allows_nil => false },
  			:bill_number => { :value => lambda { Bill.first_bill? ? BillCode.active.first.bill_code_start_from : ( Bill.last.bill_number + 1 ) }, :allows_nil => false }  			
  	# ----------------------------------------------------------------------
  	before_destroy { |record| BillItem.destroy_all "bill_id = #{record.id}" }
        # ----------------------------------------------------------------------
        def gross
        	gross = 0
        	self.bill_items.each do |item|
        		gross += item.bill_item_unit_gross
        	end
        	return gross
        end
        # ----------------------------------------------------------------------
        def fees
        	fees = 0
        	self.bill_items.each do |item|
        		fees += item.bill_item_unit_fee
        	end
        	return fees        	
        end  	
        # ----------------------------------------------------------------------
        def currency
        	self.company.company_country[0..1].downcase
        end
        # ----------------------------------------------------------------------
        def code
           return self.company.company_country.to_s + "-" + self.bill_code.bill_code.to_s + "-" + (sprintf '%05d', self.bill_number)	
        end
        # ----------------------------------------------------------------------
  	def self.first_bill?
  		    Bill.all.count > 0 ? false : true
  	end
        # ----------------------------------------------------------------------
        def self.produce(bill_items)
        	@bill = Bill.create(:company_id => bill_items.first.company_id, :bill_code_id => BillCode.active.first.id)
  	  	amount = 0               
  	  	bill_items.each do |bill_item|
  	  	  amount += bill_item.bill_item_unit_cost         
  	      	  bill_item.attributes = { :bill_id => @bill.id }  
  	          bill_item.save!  	          	  	  
  	  	end
  	  	@bill.attributes = { :bill_amount => amount } 
  	  	@bill.save
  	  	return @bill
        end
        # ----------------------------------------------------------------------
        def self.fees()
        	fees = 0
        	Bill.all.each do |bill|
        	  fees += bill.fees
                end
                return fees        	
        end
        # ----------------------------------------------------------------------
        def self.gross()
        	gross = 0
        	Bill.all.each do |bill|
        	  gross += bill.gross
                end
                return gross
        end
        # ----------------------------------------------------------------------
        def self.total()
        	total = 0
        	Bill.all.each do |bill|
        	  total += bill.bill_amount
                end
                return total
        end  
        # ----------------------------------------------------------------------   
        def self.daily_amount
        	daily_amount = 0
        	@time_window = Time.zone.now.beginning_of_day..Time.zone.now
        	return Bill.group_by_day(:created_at, Time.zone, @time_window).sum(:bill_amount)
        end
        # ----------------------------------------------------------------------
        def self.daily_count
        	daily_count = 0
        	@time_window = Time.zone.now.beginning_of_day..Time.zone.now
        	Bill.group_by_day(:created_at, Time.zone, @time_window).count.each do |key, val|
        		daily_count += val
        	end
        	return daily_count        	
        end

        # ----------------------------------------------------------------------
	def self.sync
	          BillItem.to_bill.group_by(&:company_id).each do |company_id, bill_items|
	             Bill.produce(bill_items)
	          end	
	end
	# ----------------------------------------------------------------------
	def self.unsent
	  Bill.where{ bill_status.eq 'draft' }.all 	
	end
	# ----------------------------------------------------------------------    
	def self.invoice
	          Bill.unsent.each do |bill|
		    UserMailer.invoice(bill).deliver
  	  	    bill.update_attributes(:bill_status => 'sent') 
  	  	    bill.update_attributes(:bill_sent_at => Time.zone.now)
	          end	
	end
	# ----------------------------------------------------------------------   
	def self.report
	      UserMailer.activity_report.deliver	
	end
	# ----------------------------------------------------------------------   
	def invoice
	    UserMailer.invoice(self).deliver
	end	
	# ----------------------------------------------------------------------    	
end
