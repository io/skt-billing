class BillCode < ActiveRecord::Base   
	# ----------------------------------------------------------------------
  	attr_accessible :bill_code, :bill_code_start_from, :bill_code_status
  	# ----------------------------------------------------------------------
  	has_many :bills   	
  	# ----------------------------------------------------------------------
  	validates :bill_code, :presence => true  
  	validates_format_of :bill_code, :with => /\A(\d{2})+(_COR)\Z/i, :message => "A Bill Code must be formatted as YY_COR!"
  	validates_uniqueness_of :bill_code, :message => "Bill Codes must be unique!"
  	validates_length_of :bill_code, :minimum => 6, :maximum => 6, :allow_blank => false, :message => "Invalid length!" 
  	validate :on_billing_cycle, :if => :active?
  	# ----------------------------------------------------------------------
  	scope :active, -> { where(:bill_code_status => 1) }
  	# ----------------------------------------------------------------------
  	default_values :bill_code => { :value => lambda { Time.now.strftime("%y") + '_COR' }, :allows_nil => false },
  			:bill_code_start_from => { :value => lambda { sprintf '%05d', 1 }, :allows_nil => false },
  			:bill_code_status => { :value => 0, :allows_nil => false }
  	
  	# ----------------------------------------------------------------------    
  	before_destroy { |record| Bill.destroy_all "bill_code_id = #{record.id}"   }
  	# ---------------------------------------------------------------------- 
  	def on_billing_cycle
  		if BillCode.active.count > 0
  			errors.add(:bill_code_status, "There can only be 1 Bill Code active at a time!")
  		end  		
  	end                             
        # ----------------------------------------------------------------------
  	private
  	  # --------------------------------------------------------------------
  	  def active?
  	  	self.bill_code_status == 1 ? true : false
  	  end
        # ----------------------------------------------------------------------
end
