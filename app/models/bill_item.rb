class BillItem < ActiveRecord::Base
	# ----------------------------------------------------------------------
  	attr_accessible :company_id, :bill_item_vat, :client_id, :bill_id, :bill_item_description, :bill_item_discount, :bill_item_quantity, :bill_item_unit_cost, :bill_item_unit_gross, :bill_item_unit_fee, :bill_item_processed_at, :bill_item_transaction_id, :bill_item_currency
  	# ----------------------------------------------------------------------
  	validates :bill_item_description, :bill_item_transaction_id, :bill_item_currency, :bill_item_unit_cost, :presence => true
  	validates :bill_item_discount, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100}
  	validates :bill_id, :presence => true, :unless => :nested
  	attr_accessor :nested
  	# ----------------------------------------------------------------------
  	belongs_to :bill
  	belongs_to :company
  	belongs_to :client
  	# ----------------------------------------------------------------------
  	default_values :bill_id => { :value => -1, :allows_nil => false },
  			:bill_item_discount => { :value => 0, :allows_nil => false },
  			:bill_item_quantity => { :value => 1, :allows_nil => false },
  			:bill_item_processed_at => { :value => lambda { Time.zone.now.strftime("%d-%m-%Y %H:%M") }, :allows_nil => false },
  			:bill_item_description => { :value => "Ricarica Servizio TELEFREE (Paypal)", :allows_nil => false } 
  	# ----------------------------------------------------------------------
	def self.to_bill
		 BillItem.where{bill_id.eq -1}
	end
	# ----------------------------------------------------------------------   	
	def self.sync	
		  Payment.valid.to_bill.each do |payment|		  	
                        client = Client.find_or_create_by_client_email(:client_country => payment.residence_country, :client_email => payment.payer_email , :client_name => (payment.first_name.to_s + ' ' + payment.last_name.to_s).titleize )                                                
                        company = Company.find_or_create_by_company_email('unknown@skt.com') if !( company = Company.find_by_company_email(payment.account.address.email))		  			 
		  	BillItem.create(:bill_item_vat => payment.tax, :bill_item_unit_cost => payment.amount, :bill_item_unit_fee => payment.fee, :bill_item_unit_gross => payment.gross, :bill_item_currency => payment.currency.to_s, :bill_item_transaction_id => payment.transaction_id.to_s, :bill_item_processed_at => payment.shipped_at, :client_id => client.id, :company_id => company.id)
		  end 
	end
	# ----------------------------------------------------------------------
end
