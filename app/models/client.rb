class Client < ActiveRecord::Base
	# ------------------------------------------------------------------------------
  	attr_accessible :client_country, :client_email, :client_name
  	# ------------------------------------------------------------------------------
  	validates :client_country, :client_email, :client_name, :presence => true
  	validates :client_email, :format => { :with => /^([A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})?$/, :message => 'Email must be valid!' }        
  	# ------------------------------------------------------------------------------  	
  	has_many :bill_items
  	has_many :bills, :through => :bill_items
        # ------------------------------------------------------------------------------
  	#default_values :client_country => { :value => 'ITA', :allows_nil => false },
  	# 		:client_email => { :value => 'unknown@paypal.com', :allows_nil => false },
  	#		:client_name => { :value => 'Unknown', :allows_nil => false }    
        # ------------------------------------------------------------------------------
        # ----------------------------------------------------------------------
        def self.daily_count
        	daily_count = 0
        	@time_window = Time.zone.now.beginning_of_day..Time.zone.now
        	Client.group_by_day(:created_at, Time.zone, @time_window).count.each do |key, val|
        		daily_count += val
        	end
        	return daily_count        	
        end        
end
