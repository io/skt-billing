class Company < ActiveRecord::Base       
	# ----------------------------------------------------------------------
  	attr_accessible :company_address, :company_country, :company_name, :company_vat, :company_email, :company_phone
  	# ----------------------------------------------------------------------
  	validates :company_address, :company_country, :company_name, :company_vat, :company_email, :presence => true
  	validates :company_email, :format => { :with => /^([A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})?$/, :message => 'Email must be valid!' }
  	# ----------------------------------------------------------------------                            
  	has_many :bill_items
  	has_many :bills
        # ----------------------------------------------------------------------
  	#default_values :company_address => { :value => 'Unknown', :allows_nil => false },        
  	# 		:company_country => { :value => 'ITA', :allows_nil => false },
  	#		:company_name => { :value => 'Unknown', :allows_nil => false },
  	#		:company_vat => { :value => 'Unknown', :allows_nil => false },
  	#		:company_email => { :value => 'unknown@skt.com', :allows_nil => false },
  	#		:company_phone => { :value => 'Unknown', :allows_nil => false }       
  	# ----------------------------------------------------------------------
	def self.sync
		begin                                                                
		  Account.valid.each do |account|
		    address = account.address          
		    Company.find_or_create_by_company_email :company_email => address.email, :company_country => address.direction.code, :company_address => address.address + " - " + address.city + " - CAP " + address.postcode, :company_name => (account.first_name + " " + account.last_name).titleize, :company_phone => address.phone ? address.phone : address.mob_phone, :company_vat => account.vat_number		    		    	  
		  end                                                         
		  return true		                                        
	        rescue                                                          
	          return false                                                  
	        end                                                          
	end                                                                     
        # ----------------------------------------------------------------------  
end
