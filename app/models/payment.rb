class Payment < ActiveRecord::Base
	# ---------------------------------------------------------------------- 
	belongs_to :account
	belongs_to :bill
	# ---------------------------------------------------------------------- 
	def self.valid
		Payment.where{payer_email.not_eq 'NULL'}
	end
	# ---------------------------------------------------------------------- 
	def self.to_bill
		Payment.where{updated_at.eq self.created_at}
	end
	# ---------------------------------------------------------------------- 	
end
