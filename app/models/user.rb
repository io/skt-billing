class User < ActiveRecord::Base            
	# ----------------------------------------------------------------------
	rolify
	# ----------------------------------------------------------------------
	devise :database_authenticatable, :registerable, :confirmable, :recoverable, :rememberable, :trackable, :validatable
	# ----------------------------------------------------------------------	
	attr_accessible :role_ids, :as => :admin                                
	attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :created_at, :updated_at
	# ----------------------------------------------------------------------	
	def role?(role_sym)                                                     
	  roles.any? { |r| r.name.underscore.to_sym == role_sym }               
	end                                                                     
	# ----------------------------------------------------------------------
	# no password is required when the account is created; validate password when the user sets one
	# ----------------------------------------------------------------------
	validates_confirmation_of :password                                     
	def password_required?                                                  
	  if !persisted?                                                        
	    !(password != "")                                                   
	  else                                                                  
	    !password.nil? || !password_confirmation.nil?                       
	  end                                                                   
	end                                                                     
	# ----------------------------------------------------------------------
	# Send reset password instructions                                      
	# ----------------------------------------------------------------------
	def send_reset_password_instructions                                    
	  if self.confirmed?                                                    
	    super                                                               
	  else                                                                  
	    errors.add :base, "You must receive an invitation before you set your password."
	  end
	end  
	# ----------------------------------------------------------------------
	# override Devise method: no password confirmation required             
	# ----------------------------------------------------------------------
	def confirmation_required?                                              
	  false                                                                 
	end                                                                     
	# ----------------------------------------------------------------------
	# override Devise method: can auth only if confirmed or confirmation period has not expired
	# ----------------------------------------------------------------------
	def active_for_authentication?                                          
	  confirmed? || confirmation_period_valid?                              
	end                                                                     
	# ----------------------------------------------------------------------
	# new function to set the password                                      
	# ----------------------------------------------------------------------
	def attempt_set_password(params)                                        
	  p = {}                                                                
	  p[:password] = params[:password]                                      
	  p[:password_confirmation] = params[:password_confirmation]            
	  update_attributes(p)                                                  
	end                                                                     
	# ----------------------------------------------------------------------
	# new function to determine whether a password has been set             
	# ----------------------------------------------------------------------
	def has_no_password?                                                    
	  self.encrypted_password.blank?                                        
	end                                                                     
	# ----------------------------------------------------------------------
	# new function to provide access to protected method pending_any_confirmation
	# ----------------------------------------------------------------------
	def only_if_unconfirmed                                                 
	  pending_any_confirmation {yield}                                      
	end                                                                     
	# ----------------------------------------------------------------------
end
