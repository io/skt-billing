Tabulous.setup do

  tabs(:app) do	  

    companies_tab do
      text          { 'Companies' }
      link_path     { companies_path }
      visible_when  { true }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('companies') }
    end 
    
    clients_tab do
      text          { 'Clients' }
      link_path     { clients_path }
      visible_when  { true }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('clients') }
    end   
    
    bill_items_tab do
      text          { 'Bill Items' }
      link_path     { bill_items_path }
      visible_when  { true }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('bill_items') }
    end      
    
    bills_tab do
      text          { 'Bills' }
      link_path     { bills_path }
      visible_when  { true }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('bills') }
    end 
    
       
  end
  
  tabs(:admin) do
  	  
    bill_codes_tab do
      text          { 'Bill Codes' }
      link_path     { bill_codes_path }
      visible_when  { current_user.has_role? :admin or current_user.has_role? :owner  }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('bill_codes') }
    end     
    
    users_tab do
      text          { 'Users' }
      link_path     { users_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('users') }
    end    
    
  end
  
  tabs(:mor) do
  	
    accounts_tab do
      text          { 'Accounts' }
      link_path     { accounts_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('accounts') }
    end    	  
    
    payments_tab do
      text          { 'Payments' }
      link_path     { payments_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('payments') }
    end    	  
  		  

    addresses_tab do
      text          { 'Addresses' }
      link_path     { addresses_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('addresses') }
    end

    directions_tab do
      text          { 'Directions' }
      link_path     { directions_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('directions') }
    end

    currencies_tab do
      text          { 'Currencies' }
      link_path     { currencies_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('currencies') }
    end




  end

  customize do

    # which class to use to generate HTML
    # :default, :html5, :bootstrap, or :bootstrap_pill
    # or create your own renderer class and reference it here
    renderer :custom

    # whether to allow the active tab to be clicked
    # defaults to true
    # active_tab_clickable true

    # what to do when there is no active tab for the currrent controller action
    # :render -- draw the tabset, even though no tab is active
    # :do_not_render -- do not draw the tabset
    # :raise_error -- raise an error
    # defaults to :do_not_render
    when_action_has_no_tab :render

    # whether to always add the HTML markup for subtabs, even if empty
    # defaults to false
    # render_subtabs_when_empty false

  end



end
