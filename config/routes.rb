SktBilling::Application.routes.draw do


  resources :companies


  resources :clients


  resources :bill_codes


  resources :bills


  resources :payments


  resources :directions


  resources :currencies


  resources :addresses


  resources :accounts


  resources :bill_items


  resources :bill_codes
  resources :bill_items, :only => [:index]
  resources :companies, :only => [:index, :update]
  resources :clients, :only => [:index, :update]
  resources :bills, :only => [:index, :show]
  resources :accounts, :only => [:index]
  resources :payments, :only => [:index]
  resources :directions, :only => [:index]
  resources :currencies, :only => [:index]
  resources :addresses, :only => [:index]


  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

  devise_for :users, :controllers => { :registrations => "registrations", :confirmations => "confirmations" }
  authenticated :user do
    root :to => 'bills#index'
  end
  as :user do 
    root :to => "devise/sessions#new"    
    get "/login" => "devise/sessions#new"                                 
    post "/login" => "devise/sessions#create"
    delete "/logout" => "devise/sessions#destroy"
    #get "/subscribe" => "devise/registrations#new"
    get "/reset" => "devise/passwords#new"
    put "/reset" => "devise/passwords#edit"    
    #match '/user/confirmation' => 'confirmations#update', :via => :put, :as => :update_user_confirmation
  end 

  resources :users
  match '*a', :to => 'errors#not_found'
  match '/404', :to => 'errors#not_found'
  match '/422', :to => 'errors#server_error'
  match '/500', :to => 'errors#server_error'
end