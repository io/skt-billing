# RAILS_ROOT/config/unicorn.rb
# Search for "# SET ME!" and replace these with your own settings!.
 
# Set environment to development unless something else is specified
RAILS_ENV = ENV["RAILS_ENV"] || "development"
RAILS_ROOT = ENV["RAILS_ROOT"] || "/srv/billing"

if ENV['UNICORN']
	
  if ENV['RUBY_HOME'] && ENV['RUBY_HOME'].include?('rvm')
    begin
      rvm_path = File.dirname(File.dirname(ENV['RUBY_HOME']))
      rvm_lib_path = File.join(rvm_path, 'lib')
      $LOAD_PATH.unshift rvm_lib_path
      require 'rvm'
      RVM.use_from_path! RAILS_ROOT
    rescue LoadError
      raise "RVM ruby lib is currently unavailable."
    end
  end
   
  ENV['BUNDLE_GEMFILE'] = File.expand_path('../Gemfile', File.dirname(__FILE__))
  require 'bundler/setup'
   
  pid "#{RAILS_ROOT}/tmp/pids/unicorn.pid"
  
  # load the scheduler init script
  require "#{RAILS_ROOT}/config/initializers/rufus_scheduler"
  # path to the scheduler pid file
  scheduler_pid_file = "#{RAILS_ROOT}/tmp/pids/scheduler.pid"
    
  # See http://unicorn.bogomips.org/Unicorn/Configurator.html for complete
  # documentation.
  worker_processes 4
   
  
  listen "#{RAILS_ROOT}/tmp/sockets/unicorn.sock", :backlog => 1024
  GC.respond_to?(:copy_on_write_friendly=) and 
  	GC.copy_on_write_friendly = true
    
  stderr_path "#{RAILS_ROOT}/log/unicorn.stderr.log"
  stdout_path "#{RAILS_ROOT}/log/unicorn.stdout.log"   
    
   
  # Preload our app for more speed
  preload_app true
   
  GC.respond_to?(:copy_on_write_friendly=) and
    GC.copy_on_write_friendly = true
   
  # nuke workers after 30 seconds instead of 60 seconds (the default)
  timeout 30
   
  # Production specific settings
  if RAILS_ENV == "production"
    # Help ensure your application will always spawn in the symlinked
    # "current" directory that Capistrano sets up.
    working_directory RAILS_ROOT
   
    # feel free to point this anywhere accessible on the filesystem
    #user 'deployer', 'staff'
    #shared_path = # SET ME!
   
    #stderr_path "#{shared_path}/log/unicorn.stderr.log"
    #stdout_path "#{shared_path}/log/unicorn.stdout.log"
  end
   
  before_fork do |server, worker|
    # the following is highly recomended for Rails + "preload_app true"
    # as there's no need for the master process to hold a connection
    if defined?(ActiveRecord::Base)
      ActiveRecord::Base.connection.disconnect!
    end
   
    # Before forking, kill the master process that belongs to the .oldbin PID.
    # This enables 0 downtime deploys.
    old_pid = "#{RAILS_ROOT}/tmp/pids/unicorn.pid.oldbin"
    if File.exists?(old_pid) && server.pid != old_pid
      begin
        Process.kill("QUIT", File.read(old_pid).to_i)
      rescue Errno::ENOENT, Errno::ESRCH
        # someone else did our job for us
      end
    end
    
    Signal.trap 'TERM' do
      puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
      Process.kill 'QUIT', Process.pid
    end
      
  end
   
  after_fork do |server, worker|
    # the following is *required* for Rails + "preload_app true",
    if defined?(ActiveRecord::Base)
      ActiveRecord::Base.establish_connection
    end
   
    Signal.trap 'TERM' do  
      puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to sent QUIT'
    end
    
    if defined?(ActiveRecord::Base) and 
    	  ActiveRecord::Base.establish_connection
      child_pid = server.config[:pid].sub('.pid', ".#{worker.nr}.pid")
      system("echo #{Process.pid} > #{child_pid}")
    
      # run scheduler initialization
      Scheduler::start_unless_running scheduler_pid_file
   end
   
  end        
end

