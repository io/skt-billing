class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :direction_id
      t.string :state
      t.string :county
      t.string :city
      t.string :postcode
      t.string :address
      t.string :phone
      t.string :mob_phone
      t.string :fax
      t.string :email

      t.timestamps
    end
  end
end
