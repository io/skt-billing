class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :name
      t.string :full_name
      t.float :exchange_rate
      t.integer :active
      t.timestamp :last_update
      t.integer :curr_update
      t.integer :curr_edit

      t.timestamps
    end
  end
end
