class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :paymenttype
      t.float :amount
      t.string :currency
      t.string :email
      t.datetime :date_added
      t.integer :completed
      t.string :transaction_id
      t.datetime :shipped_at
      t.float :fee
      t.float :gross
      t.string :first_name
      t.string :last_name
      t.string :payer_email
      t.string :residence_country
      t.string :payer_status
      t.float :tax
      t.string :account_id
      t.string :pending_reason
      t.float :vat_percent
      t.integer :owner_id
      t.integer :card
      t.string :hash_legacy
      t.string :bill_nr
      t.binary :description
      t.integer :provider_id

      t.timestamps
    end
  end
end
