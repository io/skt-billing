class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.text :bill_notes
      t.string :bill_status
      t.datetime :bill_sent_at
      t.datetime :bill_due_date
      t.integer :bill_code_id
      t.float :bill_vat
      t.integer :bill_number
      t.integer :company_id
      t.float :bill_amount

      t.timestamps
    end
  end
end
