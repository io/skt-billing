class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :client_name
      t.string :client_email
      t.string :client_country

      t.timestamps
    end
  end
end
