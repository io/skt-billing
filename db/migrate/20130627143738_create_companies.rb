class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :company_name
      t.string :company_country
      t.string :company_address
      t.string :company_vat
      t.string :company_email
      t.string :company_phone

      t.timestamps
    end
  end
end
