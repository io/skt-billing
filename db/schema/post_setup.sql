ALTER TABLE payments   
  CHANGE `user_id` `account_id` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NULL,
  CHANGE `hash` `hash_legacy` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NULL;
  
RENAME TABLE users TO accounts;  

