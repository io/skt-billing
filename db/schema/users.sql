-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: 37.59.1.175    Database: mor
-- ------------------------------------------------------
-- Server version	5.5.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `usertype` varchar(20) NOT NULL DEFAULT 'user',
  `logged` tinyint(4) NOT NULL DEFAULT '0',
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `calltime_normative` double NOT NULL DEFAULT '3',
  `show_in_realtime_stats` tinyint(4) NOT NULL DEFAULT '0',
  `balance` double NOT NULL DEFAULT '0',
  `frozen_balance` double NOT NULL DEFAULT '0',
  `lcr_id` bigint(20) NOT NULL DEFAULT '0',
  `postpaid` tinyint(4) NOT NULL DEFAULT '1',
  `blocked` tinyint(4) NOT NULL DEFAULT '0',
  `tariff_id` bigint(20) NOT NULL DEFAULT '0',
  `month_plan_perc` double NOT NULL DEFAULT '0',
  `month_plan_updated` datetime DEFAULT '0000-00-00 00:00:00',
  `sales_this_month` int(11) NOT NULL DEFAULT '0',
  `sales_this_month_planned` int(11) NOT NULL DEFAULT '0',
  `show_billing_info` tinyint(4) NOT NULL DEFAULT '1',
  `primary_device_id` int(11) NOT NULL DEFAULT '0',
  `credit` double DEFAULT '-1',
  `clientid` varchar(30) DEFAULT NULL,
  `agreement_number` varchar(20) DEFAULT NULL,
  `agreement_date` date DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `taxation_country` int(11) DEFAULT NULL,
  `vat_number` varchar(30) DEFAULT NULL,
  `vat_percent` double NOT NULL DEFAULT '0',
  `address_id` int(11) DEFAULT NULL,
  `accounting_number` varchar(30) DEFAULT NULL,
  `owner_id` int(11) DEFAULT '0',
  `hidden` tinyint(4) DEFAULT '0',
  `allow_loss_calls` int(11) DEFAULT '0',
  `vouchers_disabled_till` datetime DEFAULT '2000-01-01 00:00:00',
  `uniquehash` varchar(10) DEFAULT NULL,
  `c2c_service_active` tinyint(4) DEFAULT '0',
  `temporary_id` int(11) DEFAULT NULL,
  `send_invoice_types` int(11) DEFAULT '1',
  `call_limit` int(11) DEFAULT '0',
  `c2c_call_price` double DEFAULT NULL,
  `sms_tariff_id` int(11) DEFAULT NULL,
  `sms_lcr_id` int(11) DEFAULT NULL,
  `sms_service_active` int(11) DEFAULT '0',
  `cyberplat_active` int(1) DEFAULT '0',
  `call_center_agent` int(11) DEFAULT '0',
  `generate_invoice` tinyint(4) DEFAULT '1',
  `tax_1` double DEFAULT '0',
  `tax_2` double DEFAULT '0',
  `tax_3` double DEFAULT '0',
  `tax_4` double DEFAULT '0',
  `block_at` date DEFAULT '2008-01-01',
  `block_at_conditional` tinyint(4) DEFAULT '15',
  `block_conditional_use` tinyint(4) DEFAULT '0',
  `recording_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `recording_forced_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `recordings_email` varchar(50) DEFAULT NULL,
  `recording_hdd_quota` int(11) NOT NULL DEFAULT '100',
  `warning_email_active` tinyint(4) NOT NULL DEFAULT '0',
  `warning_email_balance` double NOT NULL DEFAULT '0',
  `warning_email_sent` tinyint(4) DEFAULT '0',
  `tax_id` int(11) NOT NULL DEFAULT '0',
  `invoice_zero_calls` tinyint(4) NOT NULL DEFAULT '1',
  `acc_group_id` int(11) NOT NULL DEFAULT '0',
  `hide_destination_end` tinyint(4) DEFAULT '-1',
  `warning_email_hour` int(11) DEFAULT '-1' COMMENT 'warning balance email sending at hour',
  `warning_balance_call` int(11) DEFAULT '0' COMMENT 'should system play warning balance on every call?',
  `warning_balance_sound_file_id` int(11) DEFAULT '0' COMMENT 'which file to play when balance drops lower then set value',
  `own_providers` int(11) DEFAULT '0' COMMENT 'should allow provider to have own providers?',
  `ignore_global_monitorings` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Should user be affected by global monitoring',
  `currency_id` int(11) DEFAULT '1' COMMENT 'User default currency ID',
  `quickforwards_rule_id` int(11) DEFAULT '0' COMMENT 'User quickforwards rule ID',
  `spy_device_id` int(11) DEFAULT '0' COMMENT 'ChanSpy device ID',
  `time_zone` float DEFAULT '0',
  `minimal_charge` int(11) NOT NULL DEFAULT '0' COMMENT '0 means that there is no minimal chrage set/enabled, in this case it wouldnt be logical to set it to 0. By default minimal charge is disabled, hence default value is 0. NOT NULL is set because so that there wouldnt be any ambiguity between NULL and 0 valu',
  `minimal_charge_start_at` datetime DEFAULT NULL COMMENT 'minimal charge date does not have any sence if minimal charge option is disabled, so this value can be NULL and by default it is NULL, because minimal charge is disabled by default',
  `webphone_allow_use` int(11) DEFAULT '0' COMMENT 'webphone enabled for user',
  `webphone_device_id` int(11) DEFAULT '0' COMMENT 'webphone device id',
  `responsible_accountant_id` int(11) DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `owner_id_index` (`owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=641 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-04  1:06:57
