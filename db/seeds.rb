# This file should contain all the record creation needed to seed the database with its default values.
# Run rake db:seed (or create alongside the db with db:setup).

puts 'ROLES'
YAML.load(ENV['ROLES']).each do |role|
  Role.find_or_create_by_name({ :name => role }, :without_protection => true)
  puts 'role: ' << role
end
puts 'DEFAULT USERS'
user = User.find_or_create_by_email :name => ENV['ADMIN_NAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup
puts 'user: ' << user.name
user.add_role :admin
user.skip_confirmation!
user.save!
user = User.find_or_create_by_email :name => ENV['OWNER_NAME'].dup, :email => ENV['OWNER_EMAIL'].dup, :password => ENV['OWNER_PASSWORD'].dup, :password_confirmation => ENV['OWNER_PASSWORD'].dup
puts 'user: ' << user.name
user.add_role :owner
user.skip_confirmation!
user.save!
user = User.find_or_create_by_email :name => ENV['USER_NAME'].dup, :email => ENV['USER_EMAIL'].dup, :password => ENV['USER_PASSWORD'].dup, :password_confirmation => ENV['USER_PASSWORD'].dup
puts 'user: ' << user.name
user.add_role :support
user.skip_confirmation!
user.save!


