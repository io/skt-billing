begin

  namespace :app do
    
    namespace :assets do 
    
      task :reset => :environment do
        Rake::Task['assets:clean'].invoke
        Rake::Task['assets:precompile'].invoke        
      end
    
    end    
    
    namespace :db do
    
      task :reset => :environment do
       Rake::Task['db:drop'].invoke
       Rake::Task['db:create'].invoke     
       Rake::Task['db:migrate'].invoke     
       Rake::Task['app:db:seed'].invoke
       Rake::Task['db:seed:bill_codes'].invoke
       Rake::Task['db:seed'].invoke  
      end
    
      task :set_env => :environment do
        ENV['DB_NAME']=ENV['APP_NAME']
      end
    
      task :pre_sync => :environment do  
        sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/seeds/pre_sync.sql'
      end
      
      task :post_sync => :environment do  
        sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/seeds/post_sync.sql'
      end      
      
      task :pre_setup => :environment do  
        sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/schema/pre_setup.sql'
      end
      
      task :post_setup => :environment do
        sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/schema/post_setup.sql'
      end         
    
      task :i_create => :environment do  
         ::SRC_TBL.each do |t|
           sh 'mysqldump $REMOTE_DB_NAME ' + t[0] + ' -h $REMOTE_DB_HOST -u $REMOTE_DB_USR -p"$REMOTE_DB_PWD" --no-data --no-create-db --skip-add-drop-table --skip-add-locks > db/schema/' + t[0] + '.sql'
           sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD" < db/schema/' + t[0] + '.sql'
          
         end         
      end
      
      task :i_seed => :environment do
        ::SRC_TBL.each do |t|
          sh 'mysqldump $REMOTE_DB_NAME ' + t[0] + ' -h $REMOTE_DB_HOST -u $REMOTE_DB_USR -p"$REMOTE_DB_PWD" --complete-insert --no-create-info --compact --where="' + t[1] + '" > db/seeds/' + t[0] + '.sql'
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD" < db/seeds/' + t[0] + '.sql'
        end       	
      end
      
      task :i_empty => :environment do
        ::DST_TBL.each do |t|
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  -e "TRUNCATE TABLE ' + t + '"'
        end      	
      end
            
      task :i_sync => :environment do
        ::SYN_TBL.each do |t|
          sh 'mysqldump $REMOTE_DB_NAME ' + t[0] + ' -h $REMOTE_DB_HOST -u $REMOTE_DB_USR -p"$REMOTE_DB_PWD" --complete-insert --no-create-info --skip-add-drop-table --where="' + t[1] + '" > db/seeds/' + t[0] + '_sync.sql'
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/seeds/' + t[0] + '_sync.sql'
        end      	
      end
      
      task :sync => [:set_env, :i_empty, :pre_sync, :i_sync, :post_sync]
      task :seed => [:set_env, :pre_sync, :i_seed, :post_sync]
      task :create => [:set_env, :pre_setup, :i_create, :post_setup]
      
    end    
    
  end
  
end
