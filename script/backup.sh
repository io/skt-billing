#!/bin/sh

rm -Rfv /home/$APP_NAME/
mkdir -v /home/$APP_NAME

cp Gemfile /home/$APP_NAME/
cp -Rfv app/ /home/$APP_NAME/
cp -Rfv config/ /home/$APP_NAME/
cp -Rfv --parents db/schema/ /home/$APP_NAME/
cp -Rfv --parents db/seeds/ /home/$APP_NAME/

rm -v /home/$APP_NAME/app/controllers/application_*.rb
rm -v /home/$APP_NAME/app/controllers/errors_*.rb
rm -v /home/$APP_NAME/config/cucumber.yml
rm -v /home/$APP_NAME/config/boot.rb
rm -v /home/$APP_NAME/config/initializers/backtrace_silencers.rb
rm -v /home/$APP_NAME/config/initializers/generators.rb
rm -v /home/$APP_NAME/config/initializers/mime_types.rb
rm -v /home/$APP_NAME/config/initializers/rolify.rb
rm -v /home/$APP_NAME/config/initializers/secret_token.rb
rm -v /home/$APP_NAME/config/initializers/session_store.rb
rm -v /home/$APP_NAME/config/initializers/simple_form.rb
rm -v /home/$APP_NAME/config/initializers/wicked_pdf.rb   
rm -v /home/$APP_NAME/config/initializers/wrap_parameters.rb

zip -9 -r /home/$APP_NAME_git.zip /home/$APP_NAME_git/
zip -9 -r /home/$APP_NAME.zip /home/$APP_NAME/
                                              
