#!/bin/sh     

rm -fv /srv/$APP_NAME/app/assets/stylesheets/*.scss
rm -fv /srv/$APP_NAME/app/assets/javascripts/*.coffee   
rm -fv /srv/$APP_NAME/app/helpers/accounts*
rm -fv /srv/$APP_NAME/app/helpers/addresses*
rm -fv /srv/$APP_NAME/app/helpers/currencies*
rm -fv /srv/$APP_NAME/app/helpers/directions*
rm -fv /srv/$APP_NAME/app/helpers/payments*
rm -fv /srv/$APP_NAME/app/helpers/bill*
rm -fv /srv/$APP_NAME/app/helpers/clients*
rm -fv /srv/$APP_NAME/app/helpers/companies*

rm -fv /srv/$APP_NAME/app/views/accounts/_form* 
rm -fv /srv/$APP_NAME/app/views/accounts/edit* 
rm -fv /srv/$APP_NAME/app/views/accounts/new*
rm -fv /srv/$APP_NAME/app/views/accounts/show*
rm -fv /srv/$APP_NAME/app/views/bill_codes/edit*
rm -fv /srv/$APP_NAME/app/views/bill_codes/new* 
rm -fv /srv/$APP_NAME/app/views/bill_codes/show* 
rm -fv /srv/$APP_NAME/app/views/addresses/edit*    
rm -fv /srv/$APP_NAME/app/views/addresses/new* 
rm -fv /srv/$APP_NAME/app/views/addresses/show*
rm -fv /srv/$APP_NAME/app/views/addresses/_form*   
rm -fv /srv/$APP_NAME/app/views/bills/edit*    
rm -fv /srv/$APP_NAME/app/views/bills/new* 
rm -fv /srv/$APP_NAME/app/views/bills/_form*   
rm -fv /srv/$APP_NAME/app/views/clients/edit*    
rm -fv /srv/$APP_NAME/app/views/clients/new* 
rm -fv /srv/$APP_NAME/app/views/clients/show*
rm -fv /srv/$APP_NAME/app/views/clients/_form*   
rm -fv /srv/$APP_NAME/app/views/companies/edit*    
rm -fv /srv/$APP_NAME/app/views/companies/new* 
rm -fv /srv/$APP_NAME/app/views/companies/show*
rm -fv /srv/$APP_NAME/app/views/companies/_form*   
rm -fv /srv/$APP_NAME/app/views/currencies/edit*    
rm -fv /srv/$APP_NAME/app/views/currencies/new* 
rm -fv /srv/$APP_NAME/app/views/currencies/show*
rm -fv /srv/$APP_NAME/app/views/currencies/_form*   
rm -fv /srv/$APP_NAME/app/views/directions/edit*    
rm -fv /srv/$APP_NAME/app/views/directions/new* 
rm -fv /srv/$APP_NAME/app/views/directions/show*
rm -fv /srv/$APP_NAME/app/views/directions/_form* 
rm -fv /srv/$APP_NAME/app/views/payments/edit*    
rm -fv /srv/$APP_NAME/app/views/payments/new* 
rm -fv /srv/$APP_NAME/app/views/payments/show*
rm -fv /srv/$APP_NAME/app/views/payments/_form* 


