#!/bin/sh     

ruby script/reverse_scaffold accounts Account
ruby script/reverse_scaffold addresses Address                
ruby script/reverse_scaffold currencies Currency             
ruby script/reverse_scaffold directions Direction                    
ruby script/reverse_scaffold payments Payment              

rails g scaffold Bill bill_notes:text bill_status:string bill_sent_at:datetime bill_due_date:datetime bill_code_id:integer bill_vat:float bill_number:integer company_id:integer bill_amount:float 
rails g scaffold BillCode bill_code:string bill_code_start_from:integer bill_code_status:integer
rails g scaffold BillItem bill_item_vat:float bill_item_description:string bill_item_unit_cost:float bill_item_unit_fee:float bill_item_unit_gross:float bill_item_quantity:integer bill_item_discount:integer bill_id:integer bill_item_processed_at:datetime bill_item_transaction_id:string bill_item_currency:string client_id:integer company_id:integer
rails g scaffold Client client_name:string client_email:string client_country:string
rails g scaffold Company company_name:string company_country:string company_address:string company_vat:string company_email:string company_phone:string  



