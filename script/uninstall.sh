#!/bin/sh                               

rails destroy scaffold Address                         
rails destroy scaffold Currency             
rails destroy scaffold Direction                         
rails destroy scaffold Payment               
rails destroy scaffold Account 

rails destroy scaffold Bill
rails destroy scaffold BillCode
rails destroy scaffold BillItem
rails destroy scaffold Client
rails destroy scaffold Company

rm -fv db/schema/*
rm -fv db/seeds/*



