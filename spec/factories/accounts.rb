# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :account do
    username "MyString"
    password "MyString"
    usertype "MyString"
    logged 1
    first_name "MyString"
    last_name "MyString"
    calltime_normative 1.5
    show_in_realtime_stats 1
    balance 1.5
    frozen_balance 1.5
    lcr_id 1
    postpaid 1
    blocked 1
    tariff_id 1
    month_plan_perc 1.5
    month_plan_updated "2013-06-27 16:33:58"
    sales_this_month 1
    sales_this_month_planned 1
    show_billing_info 1
    primary_device_id 1
    credit 1.5
    clientid "MyString"
    agreement_number "MyString"
    agreement_date "2013-06-27"
    language "MyString"
    taxation_country 1
    vat_number "MyString"
    vat_percent 1.5
    address_id 1
    accounting_number "MyString"
    owner_id 1
    hidden 1
    allow_loss_calls 1
    vouchers_disabled_till "2013-06-27 16:33:58"
    uniquehash "MyString"
    c2c_service_active 1
    temporary_id 1
    send_invoice_types 1
    call_limit 1
    c2c_call_price 1.5
    sms_tariff_id 1
    sms_lcr_id 1
    sms_service_active 1
    cyberplat_active 1
    call_center_agent 1
    generate_invoice 1
    tax_1 1.5
    tax_2 1.5
    tax_3 1.5
    tax_4 1.5
    block_at "2013-06-27"
    block_at_conditional 1
    block_conditional_use 1
    recording_enabled 1
    recording_forced_enabled 1
    recordings_email "MyString"
    recording_hdd_quota 1
    warning_email_active 1
    warning_email_balance 1.5
    warning_email_sent 1
    tax_id 1
    invoice_zero_calls 1
    acc_group_id 1
    hide_destination_end 1
    warning_email_hour 1
    warning_balance_call 1
    warning_balance_sound_file_id 1
    own_providers 1
    ignore_global_monitorings 1
    currency_id 1
    quickforwards_rule_id 1
    spy_device_id 1
    time_zone 1.5
    minimal_charge 1
    minimal_charge_start_at "2013-06-27 16:33:58"
    webphone_allow_use 1
    webphone_device_id 1
    responsible_accountant_id 1
  end
end
