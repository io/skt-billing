# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address do
    direction_id 1
    state "MyString"
    county "MyString"
    city "MyString"
    postcode "MyString"
    address "MyString"
    phone "MyString"
    mob_phone "MyString"
    fax "MyString"
    email "MyString"
  end
end
