# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bill_code do
    bill_code "MyString"
    bill_code_start_from 1
    bill_code_status 1
  end
end
