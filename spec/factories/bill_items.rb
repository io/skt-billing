# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bill_item do
    bill_item_vat 1.5
    bill_item_description "MyString"
    bill_item_unit_cost 1.5
    bill_item_unit_fee 1.5
    bill_item_unit_gross 1.5
    bill_item_quantity 1
    bill_item_discount 1
    bill_id 1
    bill_item_processed_at "2013-06-27 16:37:04"
    bill_item_transaction_id "MyString"
    bill_item_currency "MyString"
    client_id 1
    company_id 1
  end
end
