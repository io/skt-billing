# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bill do
    bill_notes "MyText"
    bill_status "MyString"
    bill_sent_at "2013-06-27 16:36:30"
    bill_due_date "2013-06-27 16:36:30"
    bill_code_id 1
    bill_vat 1.5
    bill_number 1
    company_id 1
    bill_amount 1.5
  end
end
