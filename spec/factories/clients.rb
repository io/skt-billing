# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :client do
    client_name "MyString"
    client_email "MyString"
    client_country "MyString"
  end
end
