# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company do
    company_name "MyString"
    company_country "MyString"
    company_address "MyString"
    company_vat "MyString"
    company_email "MyString"
    company_phone "MyString"
  end
end
