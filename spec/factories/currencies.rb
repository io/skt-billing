# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :currency do
    name "MyString"
    full_name "MyString"
    exchange_rate 1.5
    active 1
    last_update "2013-06-27 16:35:07"
    curr_update 1
    curr_edit 1
  end
end
