# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
    paymenttype "MyString"
    amount 1.5
    currency "MyString"
    email "MyString"
    date_added "2013-06-27 16:36:13"
    completed 1
    transaction_id "MyString"
    shipped_at "2013-06-27 16:36:13"
    fee 1.5
    gross 1.5
    first_name "MyString"
    last_name "MyString"
    payer_email "MyString"
    residence_country "MyString"
    payer_status "MyString"
    tax 1.5
    account_id "MyString"
    pending_reason "MyString"
    vat_percent 1.5
    owner_id 1
    card 1
    hash_legacy "MyString"
    bill_nr "MyString"
    description ""
    provider_id 1
  end
end
