require "spec_helper"

describe BillCodesController do
  describe "routing" do

    it "routes to #index" do
      get("/bill_codes").should route_to("bill_codes#index")
    end

    it "routes to #new" do
      get("/bill_codes/new").should route_to("bill_codes#new")
    end

    it "routes to #show" do
      get("/bill_codes/1").should route_to("bill_codes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/bill_codes/1/edit").should route_to("bill_codes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/bill_codes").should route_to("bill_codes#create")
    end

    it "routes to #update" do
      put("/bill_codes/1").should route_to("bill_codes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/bill_codes/1").should route_to("bill_codes#destroy", :id => "1")
    end

  end
end
